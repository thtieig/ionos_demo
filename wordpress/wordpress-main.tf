terraform {
  required_providers {
    ionoscloud = {
      source = "ionos-cloud/ionoscloud"
      version = "6.1.4"
    }
  }
}

# Authentication
provider "ionoscloud" {
  username = "${var.ionoscloud_username}"
  password = "${var.ionoscloud_password}"
}

#####################################################################
# Webserver
resource "ionoscloud_server" "webserver01" {
  name              = "web01"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 2
  ram               = 4096
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "web01hd_os"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${filebase64("./installweb.sh")}"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    ips  = ["${ionoscloud_ipblock.reserved_ip.ips[0]}"]
    firewall_active = true
    firewall_type = "ingress"
  }
}

# WEB data volume
resource "ionoscloud_volume" "web01hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver01.id}"
  name          = "web01hd_data"
  licence_type  = "LINUX"
  size          = 50
  disk_type     = "SSD"
}

# Secondary NIC (private)
resource "ionoscloud_nic" "webserver01_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver01.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth1"
  ips = [ "${var.webserver_priv_ip}" ]
  dhcp          = true
  firewall_active = false
}

# Firewall - Open ports 22, 80, 443
resource "ionoscloud_firewall" "port_22_open_webserver" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.webserver01.id}"
  nic_id           = "${ionoscloud_server.webserver01.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

resource "ionoscloud_firewall" "port_80_open_webserver" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.webserver01.id}"
  nic_id           = "${ionoscloud_server.webserver01.primary_nic}"
  protocol         = "TCP"
  name             = "http"
  port_range_start = 80
  port_range_end   = 80
}

resource "ionoscloud_firewall" "port_443_open_webserver" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.webserver01.id}"
  nic_id           = "${ionoscloud_server.webserver01.primary_nic}"
  protocol         = "TCP"
  name             = "https"
  port_range_start = 443
  port_range_end   = 443
}


#####################################################################
# Datacenter creation
resource "ionoscloud_datacenter" "demo-dc" {
  name        = "Demo DC Wordpress"
  location    = "${var.ionoscloud_datacenter_location}"
  description = "VDC managed by Terraform"
}

# Public Network
resource "ionoscloud_lan" "public_lan" {
  name          = "Public_NTW"
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = true
}

# Private LAN
resource "ionoscloud_lan" "private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = false
  name          = "Private_LAN"
}

# Reserve IP
resource "ionoscloud_ipblock" "reserved_ip" {
  name = "reserved_ip"
  location = "${var.ionoscloud_datacenter_location}"
  size     = 1
}

#####################################################################
# Database
resource "ionoscloud_server" "database01" {
  name              = "db01"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 2
  ram               = 2048
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "db01hd_os"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${filebase64("./installdb.sh")}"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "ingress"
  }
}

# DB data volume
resource "ionoscloud_volume" "db01hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database01.id}"
  name          = "db01hd_data"
  licence_type  = "LINUX"
  size          = 10
  disk_type     = "SSD"
}

# Secondary NIC (private)
resource "ionoscloud_nic" "database01_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database01.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth1"
  ips = [ "${var.dbserver_priv_ip}" ]
  dhcp          = true
  firewall_active = false
}
