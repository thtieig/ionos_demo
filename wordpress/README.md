# Wordpress - 1 Web server + 1 Database

![Image](wp_schema.png "icon")

## What will you build

### Webserver
Apache webserver with Virtualhost setup for `mywordpress.com` website.  
This is based on Debian 10 server and uses the `cloudinit` image on IONOS Cloud.  
The server runs from a 10GB hard drive (HHD type), while the website content is stored on a second 50GB SSD disk, mounted under `/var/www/data`.  
All the initial setup is made using *cloudinit*.  
The server has internet connectivity for pulling OS updates and packages, along with ports 80, 443 and SSH (22). ALL the rest is blocked by the firewall.  
The public IP of this server is reserved, and assigned to its public network interface (eth0).  
This server can talk with the database via a second NIC (eth1). Firewall is disabled on the internal Private_LAN.  

### Database
The database server is based on Debian 10 (`cloudinit` image from IONOS Cloud).  
It runs the default *MariaDB* server.  
The server has internet connectivity ONLY for pulling OS updates and packages: firewall blocks ALL accesses, including SSH.  
This server allows connectivity to the webserver via a second NIC (eth1). Firewall is disabled on the internal Private_LAN.  

## How to run this?
First of all, this is used for demo purposes and provided without any warranty.  

Wordpress requires credentials to be set within the database and some configuration files.  
Run `gen_wp_credentials.sh` script to randomly generate the credentials and have your workspace ready to run `terraform`.  

Once terraform ends, allow a couple of minutes to the cloud init scripts to finish before browsing the webserver's public IP to complete the Wordpress setup.  

Enjoy!  


