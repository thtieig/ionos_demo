variable "ionoscloud_username" {
  type = string
  description = "IONOS username"
}

variable "ionoscloud_password" {
  type = string
  description = "IONOS password"
  sensitive = true
}

variable "ionoscloud_datacenter_location" {
  type = string
  description = "IONOS regional location for VDC: gb/lhr, us/las, us/ewr, de/fra, de/fkb"
}

variable "console_password" {
  description = "Password for root user via console"
  type = string
  sensitive = true
}

variable "ssh_pub_key" {
  description = "Public Key to be added to the VMs"
  type = string
  sensitive = true
}

variable "os_image_uuid" {
  description = "Operative System Image UUID"
  type = string
}

variable "webserver_priv_ip" {
  type = string
  description = "Webserver private ip"
  default = "10.1.1.30"
}

variable "dbserver_priv_ip" {
  type = string
  description = "Database server private ip"
  default = "10.1.1.10"
}