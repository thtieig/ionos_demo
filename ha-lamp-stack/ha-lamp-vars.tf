variable "ionoscloud_username" {
  type = string
  description = "IONOS username"
}

variable "ionoscloud_password" {
  type = string
  description = "IONOS password"
  sensitive = true
}

variable "ionoscloud_datacenter_location" {
  type = string
  description = "IONOS regional location for VDC: gb/lhr, us/las, us/ewr, de/fra, de/fkb"
}

variable "console_password" {
  description = "Password for root user via console"
  type = string
  sensitive = true
}

variable "ssh_pub_key" {
  description = "Public Key to be added to the VMs"
  type = string
  sensitive = true
}

variable "os_image_uuid" {
  description = "Operative System Image UUID"
  type = string
}

variable lb_web_loc_lan_ips {
  type    = list
  default = ["10.1.1.30", "10.1.1.31"]
}

variable web_db_loc_lan_ips {
  type    = list
  default = ["10.1.2.30", "10.1.2.31", "10.1.2.50", "10.1.2.51"]
}

variable db_replica_loc_lan_ips {
  type    = list
  default = ["10.1.3.50", "10.1.3.51"]
}
