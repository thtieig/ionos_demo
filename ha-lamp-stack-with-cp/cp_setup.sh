#!/bin/bash
# First full update and upgrade
apt update
apt -y upgrade
# Install git, Ansible Debian package and Terraform dependencies
apt install -y gnupg software-properties-common curl git ansible
# Add HashiCorp Linux repo
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
# Install Terraform
apt update
apt -y install terraform
