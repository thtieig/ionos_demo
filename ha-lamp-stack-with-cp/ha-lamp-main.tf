terraform {
  required_providers {
    ionoscloud = {
      source = "ionos-cloud/ionoscloud"
      version = "6.1.4"
    }
  }
}

# Authentication
provider "ionoscloud" {
  username = "${var.ionoscloud_username}"
  password = "${var.ionoscloud_password}"
}

#####################################################################
# Datacenter creation
resource "ionoscloud_datacenter" "demo-dc" {
  name        = "Demo HA Lamp Stack"
  location    = "${var.ionoscloud_datacenter_location}"
  description = "VDC managed by Terraform"
}

# Public Network
resource "ionoscloud_lan" "public_lan" {
  name          = "Public_NTW"
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = true
}

# lb_web_loc_lan
resource "ionoscloud_lan" "lb_web_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = false
  name          = "lb_web_loc_lan"
}

# private_lan
resource "ionoscloud_lan" "private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = false
  name          = "private_lan"
}

# db_replica_loc_lan
resource "ionoscloud_lan" "db_replica_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = false
  name          = "db_replica_loc_lan"
}

# Reserve IP
resource "ionoscloud_ipblock" "reserved_ip" {
  name = "reserved_ip"
  location = "${var.ionoscloud_datacenter_location}"
  size     = 1
}

# Network load balancer
resource "ionoscloud_networkloadbalancer" "lb01" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  name          = "LB01"
  listener_lan  = "${ionoscloud_lan.public_lan.id}"
  target_lan    = "${ionoscloud_lan.lb_web_loc_lan.id}"
  ips           =["${ionoscloud_ipblock.reserved_ip.ips[0]}"]
}

resource "ionoscloud_networkloadbalancer_forwardingrule" "httprule" {
 datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
 networkloadbalancer_id = "${ionoscloud_networkloadbalancer.lb01.id}"
 name = "HTTP"
 algorithm = "SOURCE_IP"
 protocol = "TCP"
 listener_ip = "${ionoscloud_ipblock.reserved_ip.ips[0]}"
 listener_port = "80"
   dynamic "targets" {
     for_each = var.lb_web_loc_lan_ips
     content {
        ip = targets.value
        port = "80"
        weight = "1"
        health_check {
            check = true
            check_interval = 1000
            maintenance = false
        }
     }
   }
}

resource "ionoscloud_networkloadbalancer_forwardingrule" "httsprule" {
 datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
 networkloadbalancer_id = "${ionoscloud_networkloadbalancer.lb01.id}"
 name = "HTTPS"
 algorithm = "SOURCE_IP"
 protocol = "TCP"
 listener_ip = "${ionoscloud_ipblock.reserved_ip.ips[0]}"
 listener_port = "443"
   dynamic "targets" {
     for_each = var.lb_web_loc_lan_ips
     content {
        ip = targets.value
        port = "443"
        weight = "1"
        health_check {
            check = true
            check_interval = 1000
            maintenance = false
        }
     }
   }
}

#####################################################################
# Control Plane server
### ControlPlane ###
resource "ionoscloud_server" "controlplane" {
  name              = "ControlPlane"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 1024
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "cphd_os"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${filebase64("./cp_setup.sh")}"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "INGRESS"
  }
}
# Firewall - Open port 22 ONLY - eth0 public
resource "ionoscloud_firewall" "port_22_open_controlplane" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.controlplane.id}"
  nic_id           = "${ionoscloud_server.controlplane.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

# NIC (private_lan)
resource "ionoscloud_nic" "controlplane_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.controlplane.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth2"
  ips           = [ "${var.private_lan_ips[4]}" ]
  dhcp          = true
  firewall_active = false
}

#####################################################################
# Webservers

### Web01 ###
resource "ionoscloud_server" "webserver01" {
  name              = "web01"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 2048
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "web01hd_os"
    size           = 20
    disk_type      = "HDD"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "INGRESS"
  }
}

# NIC (lb_web_loc_lan)
resource "ionoscloud_nic" "webserver01_lb_web_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver01.id}"
  lan           = "${ionoscloud_lan.lb_web_loc_lan.id}"
  name          = "eth1"
  ips           = [ "${var.lb_web_loc_lan_ips[0]}" ]
  dhcp          = true
  firewall_active = false
}

# Firewall - Open port 22 ONLY - eth0 public
resource "ionoscloud_firewall" "port_22_open_webserver01" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.webserver01.id}"
  nic_id           = "${ionoscloud_server.webserver01.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

# NIC (private_lan)
resource "ionoscloud_nic" "webserver01_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver01.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth2"
  ips           = [ "${var.private_lan_ips[0]}" ]
  dhcp          = true
  firewall_active = false
}

# WEB data volume webserver01
resource "ionoscloud_volume" "web01hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver01.id}"
  name          = "web01hd_data"
  licence_type  = "LINUX"
  size          = 20
  disk_type     = "SSD"
}

### Web02 ###
resource "ionoscloud_server" "webserver02" {
  name              = "web02"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 2048
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "web02hd_os"
    size           = 20
    disk_type      = "HDD"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "INGRESS"
  }
}

# NIC (lb_web_loc_lan)
resource "ionoscloud_nic" "webserver02_lb_web_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver02.id}"
  lan           = "${ionoscloud_lan.lb_web_loc_lan.id}"
  name          = "eth1"
  ips           = [ "${var.lb_web_loc_lan_ips[1]}" ]
  dhcp          = true
  firewall_active = false
}

# Firewall - Open port 22 ONLY - eth0 public
resource "ionoscloud_firewall" "port_22_open_webserver02" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.webserver02.id}"
  nic_id           = "${ionoscloud_server.webserver02.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

# NIC (private_lan)
resource "ionoscloud_nic" "webserver02_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver02.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth2"
  ips           = [ "${var.private_lan_ips[1]}" ]
  dhcp          = true
  firewall_active = false
}

# WEB data volume webserver02
resource "ionoscloud_volume" "web02hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.webserver02.id}"
  name          = "web02hd_data"
  licence_type  = "LINUX"
  size          = 20
  disk_type     = "SSD"
}

#####################################################################
# Databases

### DB01 ###
resource "ionoscloud_server" "database01" {
  name              = "db01"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 2
  ram               = 2048
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "db01hd_os"
    size           = 20
    disk_type      = "HDD"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "INGRESS"
  }
}

# Firewall - Open port 22 ONLY - eth0 public
resource "ionoscloud_firewall" "port_22_open_db01" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.database01.id}"
  nic_id           = "${ionoscloud_server.database01.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

# NIC (private_lan)
resource "ionoscloud_nic" "database01_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database01.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth2"
  ips           = [ "${var.private_lan_ips[2]}" ]
  dhcp          = true
  firewall_active = false
}

# NIC (db_replica_loc_lan)
resource "ionoscloud_nic" "database01_db_replica_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database01.id}"
  lan           = "${ionoscloud_lan.db_replica_loc_lan.id}"
  name          = "eth2"
  ips           = [ "${var.db_replica_loc_lan_ips[0]}" ]
  dhcp          = true
  firewall_active = false
}

# DB data volume db01
resource "ionoscloud_volume" "db01hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database01.id}"
  name          = "db01hd_data"
  licence_type  = "LINUX"
  size          = 50
  disk_type     = "SSD"
}

### DB02 ###
resource "ionoscloud_server" "database02" {
  name              = "db02"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 2
  ram               = 2048
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "db02hd_os"
    size           = 20
    disk_type      = "HDD"
  }

  nic {
    name = "eth0"
    lan  = "${ionoscloud_lan.public_lan.id}"
    dhcp = true
    firewall_active = true
    firewall_type = "INGRESS"
  }
}

# Firewall - Open port 22 ONLY - eth0 public
resource "ionoscloud_firewall" "port_22_open_db02" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.database02.id}"
  nic_id           = "${ionoscloud_server.database02.primary_nic}"
  protocol         = "TCP"
  name             = "ssh"
  port_range_start = 22
  port_range_end   = 22
}

# NIC (private_lan)
resource "ionoscloud_nic" "database02_private_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database02.id}"
  lan           = "${ionoscloud_lan.private_lan.id}"
  name          = "eth2"
  ips           = [ "${var.private_lan_ips[3]}" ]
  dhcp          = true
  firewall_active = false
}

# NIC (db_replica_loc_lan)
resource "ionoscloud_nic" "database02_db_replica_loc_lan" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database02.id}"
  lan           = "${ionoscloud_lan.db_replica_loc_lan.id}"
  name          = "eth2"
  ips           = [ "${var.db_replica_loc_lan_ips[1]}" ]
  dhcp          = true
  firewall_active = false
}

# DB data volume db02
resource "ionoscloud_volume" "db02hd_data" {
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  server_id     = "${ionoscloud_server.database02.id}"
  name          = "db02hd_data"
  licence_type  = "LINUX"
  size          = 50
  disk_type     = "SSD"
}