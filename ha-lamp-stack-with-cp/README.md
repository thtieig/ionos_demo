# Wordpress - 1 Web server + 1 Database

![Image](ha-lamp-stack.png "icon")

## What will you build:
- 1 Network Load Balancer with ports 80 and 443 open
- 1 Reserved Public IP assigned to the Load Balancer
- 2 Webservers: 1 CPU + 2GB RAM, with 20GB of HHD for the OS and 20GB of SSD disk for the web data
- 2 Database servers: (master-slave) 2 CPU + 4GB RAM, with 20GB of HHD for the OS and 50GB of SSD disk for the db data
- 1 ControlPlane server with git, ansible and terraform installed via cloud-init
- 4 separate networks:
  - _Public_NTW_: all servers have internet access to pull OS updates and download packages. ONLY port 22 is open from outside.
  - _lb_web_loc_lan_: private newtwork between the network load balancer and the webservers. All traffic is allowed - no Internet exposure.
  - _private_lan_: private newtwork where webservers, database servers and controlPlane servers communicate. Traffic from webservers should point to db01 (master) for writes and to db02 (slave) for reads. All traffic is allowed - no Internet exposure.
  - _db_replica_loc_lan_: private newtwork between the database servers for replica purposes. All traffic is allowed - no Internet exposure.  

All servers are based on Debian 11.  
All private networks have IP manually assigned.  
No software configuration has been pushed. Apache/Nginx recommended on the webservers, and MySQL/Percona on the database servers.  
Also highly recommended to use the *object storage* product to store static content, avoiding/reducing the usage of local disk on the webservers.  

Enjoy!  
