# Wordpress - 2 Web server + 1 Database

![Image](stack.png "icon")

## What will you build:
- 1 Network Load Balancer with ports 80 and 443 open (source_ip algorithm: "sticky session" alike)
- 1 Reserved Public IP assigned to the Load Balancer
- 2 Webservers: 1 CPU + 2GB RAM, with 20GB of HHD for the OS and 20GB of SSD disk for the web data
- 1 Database server: 2 CPU + 4GB RAM, with 20GB of HHD for the OS and 50GB of SSD disk for the db data
- 3 separate networks:
  - _Public_NTW_: all servers have internet access to pull OS updates and download packages. Also port 22 is open from outside.
  - _lb_web_loc_lan_: private newtwork between the network load balancer and the webservers. All traffic is allowed - no Internet exposure.
  - _web_db_loc_lan_: private newtwork between the webservers and the database servers. 

All servers are based on Ubuntu latest.  
All private networks have IP manually assigned.  
Apache + PHP installed on the web servers with Wordpress.  
MariaDB installed on the DB server - DB connection allowed on the local network.  

### How to build:
Rename `terraform.auto.tfvars.example` to `terraform.auto.tfvars`.  
Update this file accordingly with your credentials and custom variables (optionals).  
**BEFORE RUNNING TERRAFORM** make sure to run once `./gen_wp_credentials.sh` script. This script accept 1 argument, which is the *domain name* of the Wordpress site that you're going to host.  
e.g. `./gen_wp_credentials.sh supercoolwpsite.net`  
If no parameters are provided, it will create a basic `mywordpress.com` virtualhost.  

After that, standard Terraform commands: `terraform init`, `terraform plan`, `terraform apply`.  


Enjoy!  
