#!/bin/bash

DEFAULT_WORDPRESS_DOMAIN="mywordpress.com"

if [ -z $1 ]; then 
    echo "No domain name provided for your Wordpress website - using '${DEFAULT_WORDPRESS_DOMAIN}'."
    WORDPRESS_DOMAIN="${DEFAULT_WORDPRESS_DOMAIN}"
else 
    echo "Provided '$1' for your Wordpress website. Setting up."
    WORDPRESS_DOMAIN="$1"
fi


# Check if a custom private IP has been assigned to the database. If not, get the default one from terraform vars.
DBIP="$(grep ^dbserver_priv_ip terraform.auto.tfvars | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')"
if [ -z ${DBIP} ]; then
    WPDBHOST="$(grep -A 5 dbserver_priv_ip *vars.tf | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')"
else
    WPDBHOST="${DBIP}"
fi


# Configure Wordpress DB
WPDBNAME="$(echo ${WORDPRESS_DOMAIN}| tr -d '.' | tr -d '-' | fold -w14 | head -n1)"
WPDBUSER="${WPDBNAME}"
WPDBUSERPASS="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w14 | head -n1)"
MYSQLROOTPW="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w14 | head -n1)"


for i in web db ; do 
cat <<EOF > install$i.sh
#!/bin/bash
################################################
#### Generated by gen_wp_credentials script ####
################################################
WORDPRESS_DOMAIN="${WORDPRESS_DOMAIN}"
WPDBNAME="${WPDBNAME}"
WPDBUSER="${WPDBUSER}"
WPDBUSERPASS="${WPDBUSERPASS}"
MYSQLROOTPW="${MYSQLROOTPW}"
WPDBHOST="${WPDBHOST}"
################################################

EOF
cat install_$i.sh_tail >> install$i.sh
done

echo -e "
All Cloud Init scripts have been configured.
To build your '${WORDPRESS_DOMAIN}' Wordpress website, setup the 'terraform.auto.tfvars' file accordingly and use 'terraform'.
Once completed, browse the public IP of your Webserver to access the final Wordpress setup.
Enjoy!
"