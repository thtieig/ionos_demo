# README #

Collection of IONOS Terraform templates for DEMO purposes. 

### List of available templates ###

* single_cube (with firewall ON and multiple NICs)
* single_cloudserver (with firewall ON and only port 22 open)
* k8s_cluster_2_nodes
* cloudservers_cloudinit
* Web+DB Wordpress automatic installation  
* HA load balanced 2 Web nodes + DB (master-slave) setup with private networks
* HA load balanced 2 Web nodes + DB (master-slave) setup with private networks + Single Controlpanel node with git, ansible and terraform  
* Single Controlplane server with git, Ansible, Terraform and Jenkins (Jenkins requires manual final setup)
* wp-2web-1db: wordpress installation on a load balanced 2 web nodes and single db (soon with DB replication)

### How do I get set up? ###

1. Create an IONOS account (if you don't have one). This is a one-off manual operation. You can open a new account on the [IONOS UK website](https://cloud.ionos.co.uk/compute/signup).
2. Choose the folder based on what you'd like to build, and enter it.
3. Configure the Terraform variables. Within each folder you should see a `terraform.auto.tfvars.example`. Just rename it, removing the `.example` extention and update it accordingly. If you're unsure about which datacenter is available on your account, you can use [this API call](https://api.ionos.com/docs/cloud/v6/#locationsget-8llkg) to get the full list. You can use `gb/lhr` for the UK datacenter.
4. Within the selected directory, run `terraform init`, `terraform plan` and `terraform apply` to build the infrastructure.


#### NOTE ####
Please remenber to double check that the OS image/template selected is available in the datacentre that you are creating.  
The `cloudservers_cloudinit` example creates 2 servers: one using the YAML cloud init configuration and another one using a simple bash script.  

