#!/bin/bash
# First full update and upgrade
apt update
apt -y upgrade
# Install git, java, Ansible Debian package and Terraform dependencies
apt install -y gnupg software-properties-common curl git ansible openjdk-11-jdk
# Add HashiCorp Linux repo
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
# Add Jenkins repo
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt update
# Install Terraform and Jenkins
apt -y install terraform jenkins
# Setup Jenkins
systemctl enable jenkins
systemctl start jenkins