terraform {
  required_providers {
    ionoscloud = {
      source = "ionos-cloud/ionoscloud"
      version = "6.1.4"
    }
  }
}

# Authentication
provider "ionoscloud" {
  username = "${var.ionoscloud_username}"
  password = "${var.ionoscloud_password}"
}

#####################################################################
# Datacenter creation
resource "ionoscloud_datacenter" "demo-dc" {
  name        = "Demo DC ControlPanel"
  location    = "${var.ionoscloud_datacenter_location}"
  description = "VDC managed by Terraform"
}

# Public LAN
resource "ionoscloud_lan" "public_lan" {
  name          = "Public Lan"
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = true
}

#####################################################################
resource "ionoscloud_server" "controlpanel" {
  name              = "Control Panel"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 1024
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "cphd"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${filebase64("./cp_setup.sh")}"

  }

  nic {
    name = "eth0"
    lan  = ionoscloud_lan.public_lan.id
    dhcp = true
    firewall_active = true
  }
}

# Firewall - Open port 22 (SSH)
resource "ionoscloud_firewall" "port_22_open_server2" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.controlpanel.id}"
  nic_id           = "${ionoscloud_server.controlpanel.primary_nic}"
  protocol         = "TCP"
  name             = "ssh port"
  port_range_start = 22
  port_range_end   = 22
}

# Firewall - Open port 8080 (Jenkins)
resource "ionoscloud_firewall" "port_8080_open_server2" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.controlpanel.id}"
  nic_id           = "${ionoscloud_server.controlpanel.primary_nic}"
  protocol         = "TCP"
  name             = "jenkins port"
  port_range_start = 8080
  port_range_end   = 8080
}
#####################################################################