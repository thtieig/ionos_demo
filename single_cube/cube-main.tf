terraform {
  required_providers {
    ionoscloud = {
      source = "ionos-cloud/ionoscloud"
      version = "6.3.2"
    }
  }
}

# Authentication
provider "ionoscloud" {
  username = "${var.ionoscloud_username}"
  password = "${var.ionoscloud_password}"
}

# Datacenter creation
resource "ionoscloud_datacenter" "demo-dc" {
  name        = "Demo DC Cube"
  location    = "${var.ionoscloud_datacenter_location}"
  description = "VDC managed by Terraform"
}

# Public LAN
resource "ionoscloud_lan" "public_lan" {
  name          = "Public Lan"
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = true
}

# Single Ubuntu Server - latest image, firewall open only port 22
data "ionoscloud_template" "cubexs01" {
    name            = "CUBES XS"
}
resource "ionoscloud_server" "cubexs01" {
  name              = "Single Cube XS Server"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  template_uuid     = "15c6dd2f-02d2-4987-b439-9a58dd59ecc3"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image}"
  type              = "CUBE"

  volume {
    name           = "cubeXShd"
    disk_type      = "DAS"
  }

  nic {
    name = "eth0"
    lan  = ionoscloud_lan.public_lan.id
    dhcp = true
    firewall_active = true
    firewall_type     = "INGRESS"
  firewall {
      protocol          = "TCP"
      name              = "ssh_open"
      port_range_start  = 22
      port_range_end    = 22
      #source_ip         = ionoscloud_ipblock.example.ips[2]
      type              = "INGRESS"
    }
  }
}

# Firewall - Add extra custom rule on primary_nic (eth0)
resource "ionoscloud_firewall" "port_8080_open" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.cubexs01.id}"
  nic_id           = "${ionoscloud_server.cubexs01.primary_nic}"
  protocol         = "TCP"
  name             = "8080_open"
  port_range_start = 8080
  port_range_end   = 8080
  #source_mac        = "00:0a:95:9d:68:17"
  #source_ip         = ionoscloud_ipblock.example.ips[2]
  #target_ip         = ionoscloud_ipblock.example.ips[3]
  type              = "INGRESS"
}

resource "ionoscloud_nic" "eth1" {
    datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
    server_id         = "${ionoscloud_server.cubexs01.id}"
    name              = "eth1"
    lan               = "${ionoscloud_lan.public_lan.id}"
    dhcp              = true
    firewall_active   = true
    firewall_type     = "INGRESS"
}

resource "ionoscloud_nic" "eth2" {
    datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
    server_id         = "${ionoscloud_server.cubexs01.id}"
    name              = "eth2"
    lan               = "${ionoscloud_lan.public_lan.id}"
    dhcp              = true
    firewall_active   = true
    firewall_type     = "INGRESS"
}
