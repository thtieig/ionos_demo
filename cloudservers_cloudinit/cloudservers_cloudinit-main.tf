terraform {
  required_providers {
    ionoscloud = {
      source = "ionos-cloud/ionoscloud"
      version = "6.1.4"
    }
  }
}

# Authentication
provider "ionoscloud" {
  username = "${var.ionoscloud_username}"
  password = "${var.ionoscloud_password}"
}

#####################################################################
# Datacenter creation
resource "ionoscloud_datacenter" "demo-dc" {
  name        = "Demo DC Cloudinit"
  location    = "${var.ionoscloud_datacenter_location}"
  description = "VDC managed by Terraform"
}

# Public LAN
resource "ionoscloud_lan" "public_lan" {
  name          = "Public Lan"
  datacenter_id = "${ionoscloud_datacenter.demo-dc.id}"
  public        = true
}

#####################################################################
# Setup cloud init yaml template file
data "template_file" "user_data" {
  template = filebase64("./cloud_init.yaml")
}

# Debian 10 (cloud init from YAML file) Cloud Server
resource "ionoscloud_server" "cloudserver01" {
  name              = "Cloud Server YAML"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 1024
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "debian10hd"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${data.template_file.user_data.template}"

  }

  nic {
    name = "eth0"
    lan  = ionoscloud_lan.public_lan.id
    dhcp = true
    firewall_active = true
  }
}

# Firewall - Open port 22 on Server 1 YAML
resource "ionoscloud_firewall" "port_22_open_server1" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.cloudserver01.id}"
  nic_id           = "${ionoscloud_server.cloudserver01.primary_nic}"
  protocol         = "TCP"
  name             = "ssh port"
  port_range_start = 22
  port_range_end   = 22
}

#####################################################################
# Debian 10 (cloud init from BASH file) Cloud Server
resource "ionoscloud_server" "cloudserver02" {
  name              = "Cloud Server BASH"
  datacenter_id     = "${ionoscloud_datacenter.demo-dc.id}"
  cores             = 1
  ram               = 1024
  availability_zone = "AUTO"
  image_password    = "${var.console_password}"
  ssh_key_path      = [ "${var.ssh_pub_key}" ]
  image_name        = "${var.os_image_uuid}"
  
  volume {
    name           = "debian10hd_bash"
    size           = 10
    disk_type      = "HDD"
    user_data      = "${filebase64("./install_apache.sh")}"

  }

  nic {
    name = "eth0"
    lan  = ionoscloud_lan.public_lan.id
    dhcp = true
    firewall_active = true
  }
}

# Firewall - Open port 22 and 80 on Server 2 BASH
resource "ionoscloud_firewall" "port_22_open_server2" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.cloudserver02.id}"
  nic_id           = "${ionoscloud_server.cloudserver02.primary_nic}"
  protocol         = "TCP"
  name             = "ssh port"
  port_range_start = 22
  port_range_end   = 22
}

resource "ionoscloud_firewall" "port_80_open_server2" {
  datacenter_id    = "${ionoscloud_datacenter.demo-dc.id}"
  server_id        = "${ionoscloud_server.cloudserver02.id}"
  nic_id           = "${ionoscloud_server.cloudserver02.primary_nic}"
  protocol         = "TCP"
  name             = "http port"
  port_range_start = 80
  port_range_end   = 80
}

#####################################################################